<!--
	@author: Mathias Helms, Maximilian Wulf
	@Funktion: Erstellt das Menue. 
			   Es wird bei jedem Tab geguckt, ob diese Seite gerade geöffnet ist. 
			   Wenn nicht wird ein Link mit vollständigem Pfad zurückgegeben.
			   Wenn ja ein Tab mit der Klasse "selected".
-->
<?php
function getLink($link)
{
	if ($link != basename($_SERVER['PHP_SELF']))
	{
		if($link == "index.php")
		{
			echo "<li><a href='../start/index.php'";
		}
		else if($link == "show_job_list.php")
		{
			echo "<li><a href='../jobs/show_job_list.php'";
		}
		else if($link == "form_users.php")
		{
			echo "<li><a href='../users/form_users.php'";
		}
		else if($link == "kontakt.php")
		{
			echo "<li><a href='../kontakt/kontakt.php'";
		}
		else if($link == "impressum.php")
		{
			echo "<li><a href='../impressum/impressum.php'";
		}
		else if($link == "show_message_list.php")
		{
			echo "<li><a href='../messages/show_message_list.php'";
		}
		else
		{
			echo "<li><a href='../start/index.php'";
		}
	}
	else
	{
		echo "<li class=\"selected\"><a ";
	}
}
?>
<nav>
	<ul>
		<?php getLink("index.php") ?>>Home</a></li>
		<?php getLink("show_job_list.php") ?>>Babysitter gesucht</a></li>
		<?php 
			//Nur wenn ein User eingeloggt ist, wird das Nachrichtenmenü angezeigt.
			if (!empty($_SESSION["user_id"]))
			{
				getLink("show_message_list.php"); 
				echo '>Nachrichten</a></li>'; 
			}
		?>
		<?php getLink("kontakt.php") ?>>Kontakt</a></li>
		<?php getLink("impressum.php") ?>>Impressum</a></li>
	</ul>
</nav>