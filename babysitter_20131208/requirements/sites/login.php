<!--
	@author: Maximilian Wulf
	@Funktion: Hier findet der Login statt. 
			   Es wird versucht die eingegebenen Daten mit einem User aus der Datenbank abzugleichen. 
			   Wenn dies gelingt wird in die Session-Variable "user_id" die User-ID des Users eingetragen.
			   Dann findet ein Redirect auf die Startseite statt.
			   Achtung: funktioniert nur wenn in der "php.ini" Datei des Servers der Wert der Variablen "session.auto_start" auf 1 gesetzt wird.
-->
<?php
require("../../requirements/dal/login/DBLogin.php");
require("../../requirements/dal/login/Login.php");
require("../../requirements/dal/database.php");

$login = new Login();

$dbLogin = new DBLogin();

$start = "../../sites/start/index.php";

if (!empty($_POST))
{
	if (empty($_SESSION["user_id"]))
	{
		$name     = $_POST['loginname'];
		$password = $_POST['loginpassword'];
		
		$login = $dbLogin->Read($name, $password);
		
		if ($login->successful == "true")
		{
			$_SESSION["user_id"]  = $login->user_id;
			
			header("Location: ".$start);
			exit;
		}
		else
		{
			header("Location: ".$start);
			exit;
		}
	}
	else
	{
		header("Location: ".$start);
		exit;
	}
}
?>
