<!--
	@author: Maximilian Wulf
	@Funktion: Hier wird jenachdem ob der User eingeloggt ist oder nicht entschieden, welcher Code eingebunden wird.
-->
<?php
if (empty($_SESSION["user_id"]))
{
// Wenn der User noch nicht eingeloggt ist wird eine Form zum Einloggen angezeigt. 
// Die eingegeben Daten werden an die "login.php" gesendet. 
// Auch ein Link zum registrieren wird angezeigt.
	
	echo '<div id="login">';
	echo 	'<form id="loginForm" method="post" action="../../requirements/sites/login.php">';
    echo 		'<fieldset><legend>Bei Ihrem Profil anmelden:</legend>';
    echo 			'<input type="text" name="loginname" id="loginname" placeholder="Benutzername">';
	echo 			'<input type="password" name="loginpassword" id="loginpassword" placeholder="Passwort">';
	echo 			'<button id="submitButton" type="submit">OK</button>';
	echo 			'<a href="../users/form_users.php">Registrieren</a>';
    echo 		'</fieldset>';
	echo 	'</form>';
	echo '</div>';
}
else
{
// Wenn der User eingeloggt ist wird dieser Code eingefügt. 
// Es werden ein Login-Text, sowie zwei Links zum Logout sowie zum eigenen Profil angezeigt.

	echo '<div id="loggedin">';
	echo 	'<span>Sie sind angemeldet&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</span>';
	echo 	'<a href="../../requirements/sites/logout.php">Logout</a></br></br>';
	echo 	'<a href="../users/form_users.php">Profil bearbeiten</a>';
	echo '</div>';
}
?>