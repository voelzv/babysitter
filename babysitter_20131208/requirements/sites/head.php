<!--
	@author: Irina Novotny, Mathias Helms, Maximilian Wulf
	@Funktion: Erstellt den oberen Bereich der PHP-Dateien. Z.B. werden hier die Stylesheets und Javascript Dateien eingebunden, aber auch der Login und das Menue.
-->
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="content-type" content="text/html; charset=utf-8" />

<title>Babysitter-Portal</title>

<link href="../../requirements/style/style.css" rel="stylesheet" type="text/css" media="screen" />
<script language="javascript" type="text/javascript" src="../../requirements/js/function.js"></script>

</head>

<body>

<div id="page">
<header>
    <img src="../../requirements/style/img/logo.jpg"  width="128" height="128" align="left" alt="">
	<div id="head_title"><h1>Das Babysitter Portal</h1></div>
	
	<?php include("../../requirements/sites/form_login.php"); ?>	
	<?php include("../../requirements/sites/menu.php"); ?>
</header>

<section id="main">