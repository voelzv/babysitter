<!--
	@author: Maximilian Wulf
	@Funktion: Hier findet der Logout statt. Die "user_id" Session Variable wird auf "" gesetzt und es folgt der Sprung auf die Startseite.
-->
<?php
	$_SESSION["user_id"]  = "";
	
	$start = "../../sites/start/index.php";
	header("Location: ".$start);
?>
