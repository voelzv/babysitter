// @author: Mathias Helms, Maximilian Wulf
// @Funktion: Um in der JobListe beim Klick auf das Message Icon auf die richtige URL zu springen.
function onJobClick(element, e)
{
	if (element.attributes["name"].value == "jobContainer")
	{
		top.location.href="form_jobs.php?job_id="+element.attributes["jobid"].value;
	}
	else if (element.attributes["name"].value == "messageSendIcon")
	{
		top.location.href="../messages/form_messages.php?anUser_id="+element.attributes["userid"].value;
	}
	if (!e) var e = window.event;
	e.stopPropagation();
}

// @author: Mathias Helms, Maximilian Wulf
// @Funktion: Um in der Nachrichten Liste beim Klick auf das L�schen Icon auf die richtige URL zu springen und die Nachricht anzuzeigen oder zu l�schen.
function onMessageClick(element, e)
{
	if (element.attributes["name"].value == "messageContainer")
	{
		top.location.href="form_messages.php?message_id="+element.attributes["messageid"].value;
	}
	else if (element.attributes["name"].value == "messageDeleteIcon")
	{
		top.location.href="show_message_list.php?del_message_id="+element.attributes["messageid"].value;
	}
	if (!e) var e = window.event;
	e.stopPropagation();
}

// @author: Irina Novotny
// @Funktion: Guckt ob das Message Formular vollst�ndig ausgef�llt wurde.
function checkFormMessage() {
  var Fehler='';

  if (document.forms[0].vonUser_id.value=="")
    Fehler += "Bitte UserId eingeben\n";
	
  if (document.forms[0].anUser_id.value=="")
    Fehler += "Bitte UserId des Empfaengers eingeben\n";
	
  if (document.forms[0].subject.value=="")
    Fehler += "Bitte Betreff eingeben";
	
  if (document.forms[0].job_id.value=="")
    Fehler += "Bitte JobId	eingeben\n";  

  if (Fehler.length>0) 
  {
    alert("Bitte vollst\u00e4ndig ausf\u00fcllen: \n\n"+Fehler);

    return(false);
  }//if

}//function checkFormMessage()