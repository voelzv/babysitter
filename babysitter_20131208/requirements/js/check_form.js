/*  @author: Irina Novotny
		@Funktion:  Die Funktion checkForm () pr&uuml;ft die Formulareingaben
		von form_users.php und form_jobs.php auf ihre Vollst&auml;ndigkeit
		Die Funktion validEmail() pr&uuml;ft die Eingabe der Emailadresse auf 
		einen regul&auml;ren Ausdruck  */
		
function checkForm() {
  var Fehler='';

  if (document.forms[0].username.value=="")
    Fehler += "Bitte Benutzernamen eingeben\n";
	
  if (document.forms[0].password.value=="")
    Fehler += "Bitte Passwort eingeben\n";
	
  if (document.forms[0].password_repeat.value=="")
    Fehler += "Bitte Passwort wiederholen\n";
	
  if (document.forms[0].firstname.value=="")
    Fehler += "Bitte Vorname eingeben\n";
	
  if (document.forms[0].lastname.value=="")
    Fehler += "Bitte Nachname eingeben\n";

  if (document.forms[0].city.value=="")
    Fehler += "Bitte Ortschaft eingeben\n";
	
  if (document.forms[0].street.value=="")
    Fehler += "Bitte Strasse eingeben\n";
	
  if (document.forms[0].number.value=="")
    Fehler += "Bitte Hausnummer eingeben\n";
	
  if (document.forms[0].phone.value=="")
    Fehler += "Bitte Telefonnummer eingeben\n";
	
  if (document.forms[0].zipcode.value && document.forms[0].zipcode.value.length<5)
    Fehler += "Bitte f&uuml;nfstellige PLZ eingeben\n";

  if (!validEmail(document.forms[0].email_addr.value)) 
  {
    Fehler += "Bitte Emailadresse eingeben\n";
  }
  
  if (document.forms[0].email_addr.value !== document.forms[0].email_addr_repeat.value)
	Fehler += "Emailadressen stimmen nicht &uuml;berein!"  

  if (Fehler.length>0) 
  {
    alert("Bitte vollst\u00e4ndig ausf\u00fcllen: \n\n"+Fehler);

    return(false);
  }

}

function validEmail(email) {

  var Reg = "^([a-zA-Z0-9_\.\-])+\@(([a-zA-Z0-9\-])+\.)+([a-zA-Z0-9]{2,4})+$";

  var regex = new RegExp(Reg);

  return(regex.test(email));

}