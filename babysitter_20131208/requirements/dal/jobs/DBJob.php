<!--
	@author: Mathias Helms, Maximilian Wulf
	@Funktion: Stellt Methoden bereit, die Zeilen in der "jobs"-Tabelle erstellen, lesen, updaten oder loeschen. Zusätzlich können auch alle Zeilen ausgelesen werden.
-->
<?php
class DBJob
{
	function Create($job)
	{		
		$db = new Database();
		
		$db->iquery("INSERT INTO jobs (
										`user_id`,
										`subject`,
										`schedule`,
										`details`,
										`numberOfChildren`
										) VALUES (
									 '".$job->user_id."',
									 '".$job->subject."',
									 '".$job->schedule."',
									 '".$job->details."',
									 '".$job->numberOfChildren."'
									)");
	}
	
	
	function Read($job_id)
	{
		$retJob = new Job();
	
		$db = new Database();

		$resArr = $db->query("SELECT * FROM jobs WHERE job_id = ".$job_id);
		
		if(count($resArr) > 0)
		{
			$retJob->job_id = $resArr[0]['job_id'];
			$retJob->user_id = $resArr[0]['user_id'];
			$retJob->subject = $resArr[0]['subject'];
			$retJob->schedule = $resArr[0]['schedule'];
			$retJob->details = $resArr[0]['details'];
			$retJob->numberOfChildren = $resArr[0]['numberOfChildren'];
		}
		
		return $retJob;
	}
	
	
	function Update($job)
	{
		$db = new Database();
				
		$db->iquery("UPDATE jobs SET user_id = '".$job->user_id."',      
									subject = '".$job->subject."',       
									schedule = '".$job->schedule."', 
									details = '".$job->details."',  
									numberOfChildren = '".$job->numberOfChildren."'
									WHERE job_id = ".$job->job_id );
	}
	
	function Delete($job_id)
	{		
		$db = new Database();

		$db->iquery("DELETE FROM jobs WHERE job_id = ".$job_id);
	}
    
    function ReadAllJobs()
    {	
        $retArrJobList = '';
        
		$db = new Database();

		$resArr = $db->query("SELECT * FROM jobs");
		
        $i = 0;
        
		while($i < count($resArr))
		{
		  $job = new Job();  
          
		  $job->job_id = $resArr[$i]['job_id'];
		  $job->user_id = $resArr[$i]['user_id'];
		  $job->subject = $resArr[$i]['subject'];
		  $job->schedule = $resArr[$i]['schedule'];
		  $job->details = $resArr[$i]['details'];
		  $job->numberOfChildren = $resArr[$i]['numberOfChildren'];
          
          $retArrJobList[] = $job;
          
          $i++;
		}
		
		return $retArrJobList;         
    }
}
?>
