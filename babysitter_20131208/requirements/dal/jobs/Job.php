<!--
	@author: Mathias Helms
	@Funktion: Job-Klasse mit benötigten Variablen.
-->
<?php
class Job
{
	var $job_id;
	var $user_id;
	var $subject;
	var $schedule;
	var $details;
	var $numberOfChildren;
}
?>   