<!--
	@author: Mathias Helms, Maximilian Wulf
	@Funktion: Stellt Methoden bereit, die Zeilen in der "user"-Tabelle erstellen, lesen, updaten oder loeschen.
-->
<?php
class DBUser
{
	function Create($user)
	{
		$db = new Database();
		
		$db->iquery("INSERT INTO users (
									`name` ,
									`password` ,
									`firstname` ,
									`lastname` ,
									`city` ,
									`street` ,
									`number` ,
									`zipcode` ,
									`email` ,
									`phone`
										) VALUES (   
									'".$user->name."',      
									'".$user->password."',       
									'".$user->firstname."', 
									'".$user->lastname."',  
									'".$user->city."',      
									'".$user->street."',    
									'".$user->number."',    
									'".$user->zipcode."',
									'".$user->email."',
									'".$user->phone."'
									)");
	}
	
	function Read($user_id)
	{
		$retUser = new User();
	
		$db = new Database();

		$resArr = $db->query("SELECT * FROM users WHERE user_id = ".$user_id);
		
		if(count($resArr) > 0)
		{
			$retUser->user_id = $resArr[0]['user_id'];
			$retUser->name = $resArr[0]['name'];
			$retUser->password = $resArr[0]['password'];
			$retUser->email = $resArr[0]['email'];
			$retUser->firstname = $resArr[0]['firstname'];
			$retUser->lastname = $resArr[0]['lastname'];
			$retUser->city = $resArr[0]['city'];
			$retUser->street = $resArr[0]['street'];
			$retUser->number = $resArr[0]['number'];
			$retUser->zipcode = $resArr[0]['zipcode'];
			$retUser->phone = $resArr[0]['phone'];
		}
		
		return $retUser;
	}
	
	function Update($user)
	{
		$db = new Database();
		
		$db->iquery("UPDATE users SET `name`= '".$user->name."',      
									`password`= '".$user->password."',       
									`firstname`= '".$user->firstname."', 
									`lastname`= '".$user->lastname."',  
									`city`= '".$user->city."',      
									`street`= '".$user->street."',    
									`number`= '".$user->number."',    
									`zipcode`= '".$user->zipcode."',
									`email`= '".$user->email."',
									`phone`= '".$user->phone."'
									WHERE user_id = ".$user->user_id );
	}
	
	function Delete($user_id, $name, $password)
	{
		$retValue = false;
		
		$db = new Database();

		//Prüft ob Benutzername und Passwort richtig sind
		$resArr = $db->query("SELECT * FROM users WHERE user_id = ".$user_id." AND name = '".$name."' AND password = '".$password."'");
		
		if(count($resArr) > 0)
		{
			$db->iquery("DELETE FROM users WHERE user_id = ".$user_id." AND name = '".$name."' AND password = '".$password."'");
		
			$retValue = true;
		}
		
		return $retValue;
	}
	
	function ExistUserName($user_name)
	{
		$retValue = false;
	
		$db = new Database();

		$resArr = $db->query("SELECT * FROM users WHERE name = '".$user_name."'");
		
		if(count($resArr) > 0)
		{
			$retValue = true;
		}
		
		return $retValue;
	}
}
?>
