<!--
	@author: Mathias Helms
	@Funktion: User-Klasse mit benötigten Variablen.
-->
<?php
class User
{
	var $user_id;
	var $name;
	var $password;
	var $email;
	var $firstname;
	var $lastname;
	var $city;
	var $street;
	var $number;
	var $zipcode;
	var $phone;
}
?>   