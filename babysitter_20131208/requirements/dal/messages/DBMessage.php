<!-- @author: Mathias Helms, Irina Novotny, Maximilian Wulf
	 @Funktion: Stellt Methoden bereit, die Nachrichten in der "message"-Tabelle erstellen, lesen, loeschen oder eine Liste von Nachrichten zu einem User übergeben.
-->

<?php
class DBMessage
{
	function Create($message)
	{
		$db = new Database();
		
		$db->iquery("INSERT INTO messages (
									`vonUser_id`,
									`anUser_id`,
									`subject`,
									`message`
										) VALUES (
									'".$message->vonUser_id."',       
									'".$message->anUser_id."', 
									'".$message->subject."',  
									'".$message->message."'
									)");
	}//function Create($message)
	
	function Read($message_id)
	{
		$retMessage = new Message();
	
		$db = new Database();

		$resArr = $db->query("SELECT * FROM messages WHERE message_id = ".$message_id);
		
		if(count($resArr) > 0)
		{
			$retMessage->message_id = $resArr[0]['message_id'];
			$retMessage->vonUser_id = $resArr[0]['vonUser_id'];
			$retMessage->anUser_id = $resArr[0]['anUser_id'];
			$retMessage->createDate = $resArr[0]['createDate'];
			$retMessage->subject = $resArr[0]['subject'];
			$retMessage->message = $resArr[0]['message'];
			
			$retMessage->createDate = sprintf("%02d.%02d.%04d %02d:%02d",
									  substr($retMessage->createDate, 9, 2),
									  substr($retMessage->createDate, 6, 2),
									  substr($retMessage->createDate, 0, 4),
									  substr($retMessage->createDate, 11, 2),
									  substr($retMessage->createDate, 14, 2),
									  substr($retMessage->createDate, 17, 2));
		}
		
		return $retMessage;
	}
	
	function ReadMessageList($anUser_id)
	{
		$retArrMessageList = '';
        
		$db = new Database();

		$resArr = $db->query("SELECT * FROM messages WHERE anUser_id = ".$anUser_id);
		
        $i = 0;
        
		while($i < count($resArr))
		{
			$message = new Message();  
          
			$message->message_id = $resArr[$i]['message_id'];
			$message->vonUser_id = $resArr[$i]['vonUser_id'];
			$message->anUser_id = $resArr[$i]['anUser_id'];
			$message->createDate = $resArr[$i]['createDate'];
			$message->subject = $resArr[$i]['subject'];
			$message->message = $resArr[$i]['message'];
			
			$message->createDate = sprintf("%02d.%02d.%04d %02d:%02d",
									  substr($message->createDate, 9, 2),
									  substr($message->createDate, 6, 2),
									  substr($message->createDate, 0, 4),
									  substr($message->createDate, 11, 2),
									  substr($message->createDate, 14, 2),
									  substr($message->createDate, 17, 2));
		  
			$retArrMessageList[] = $message;
          
			$i++;
		}
		
		return $retArrMessageList;  
	}//function ReadMessageList($anUser_id)
		
	function Delete($message_id, $anUser_id)
	{
		$db = new Database();

		$db->iquery("DELETE FROM messages WHERE anUser_id = ".$anUser_id." AND message_id = ".$message_id);
	}//function Delete($message_id)
}
?>
