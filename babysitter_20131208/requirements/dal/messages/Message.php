<!--
	@author: Mathias Helms
	@Funktion: Message-Klasse mit benötigten Variablen.
-->
<?php
class Message
{
	var $message_id;
	var $vonUser_id;
	var $anUser_id;
	var $createDate; 
	var $subject;
	var $message;
}
?>   