<!--
	@author: Mathias Helms, Maximilian Wulf
	@Funktion: Stellt die Methoden für den Datenbankzugriff bereit.
-->
<?php
class Database
{
    var $database_name;
    var $database_user;
    var $database_pass;
    var $database_host;    
    var $database_link;
	
	function Database()
    {
		$this->database_host = "127.0.0.1";
        $this->database_user = "root";
        $this->database_pass = "";
        $this->database_name = "babysitterportal";
    }
	
	function connect()
    {
        $this->database_link = mysql_connect($this->database_host, $this->database_user, $this->database_pass) or die("Fehler: Verbindung zur Datenbank konnte nicht hergestellt werden.");
        mysql_select_db($this->database_name) or die ("Fehler: Die Datenbank konnte nicht geöffnet werden: ". $this->database_name);        
    }
	
	function disconnect()
    {
        if(isset($this->database_link)) mysql_close($this->database_link);
        else mysql_close();    
    }
	
	//Für SQL Abfragen ohne Rückgabewert (z.b. bei insert, update, delete)
	function iquery($qry)
    {
        if(!isset($this->database_link)) $this->connect();
        $temp = mysql_query($qry, $this->database_link) or die("Fehler: ". mysql_error());        
    }
	
	//Für SQL Abfragen mit Rückgabewert (z.b. bei where)
	function query($qry)
    {
        if(!isset($this->database_link)) $this->connect();
        $result = mysql_query($qry, $this->database_link) or die("Fehler: ". mysql_error());
        $returnArray = array();
        $i=0;
        while ($row = mysql_fetch_array($result, MYSQL_BOTH))
            if ($row)
				$returnArray[$i++]=$row;
		mysql_free_result($result);
        return $returnArray;
    }
	
	//"true" wenn es ein Ergebnis geben wird, "false" wenn keins
	function hasRows($qry)
	{
		if(!isset($this->database_link)) $this->connect();
        $numOfRows = mysql_num_rows(mysql_query($qry, $this->database_link));
		$result = "false";
		
		if ($numOfRows > 0)
		{
			$result = "true";
		}
		
		return $result;
	}
}

?>