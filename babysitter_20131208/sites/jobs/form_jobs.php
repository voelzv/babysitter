<!--
	@author: Mathias Helms, Maximilian Wulf, Irina Novotny
	@Funktion: Stellt eine Form bereit mit der Jobs angelegt, verändert oder gelöscht werden können.
			   u.a. wird hier auch die XML-Struktur erstellt und verarbeitet
-->
<?php
require("../../requirements/sites/head.php");
?>
<?php
require("../../requirements/dal/database.php");
require("../../requirements/dal/jobs/DBJob.php");
require("../../requirements/dal/jobs/Job.php");

$job = new Job();

$dbJob = new DBJob();

$user_id = '';

$requiredDisabled = "required";
$disabled = "";

if (!empty($_SESSION["user_id"]))
{
	$user_id = $_SESSION["user_id"];
}

if (!empty($_POST) && array_key_exists('subject', $_POST))
{
	if( $_POST['subject'] == 'create' )
	{
		$job->user_id = $user_id;
		$job->subject = $_POST['job_subject'];
		$job->schedule = GetScheduleXML(); //XML erstellen
		$job->details = $_POST['details'];
		$job->numberOfChildren = $_POST['numberOfChildren'];
				
		$dbJob->Create($job);
		
		echo '<span style="color:red;padding-left:28px;">Babysitterjob angelegt.</span>';
	}
	
	if (!empty($_GET) && array_key_exists('job_id', $_GET))
	{	
		if( $_POST['subject'] == 'update' )
		{
			$job->job_id = $_GET['job_id'];
			$job->user_id = $user_id;
			$job->subject = $_POST['job_subject'];
			$job->schedule = GetScheduleXML(); //XML erstellen
			$job->details = $_POST['details'];
			$job->numberOfChildren = $_POST['numberOfChildren'];
			
			$dbJob->Update($job);
			
			echo '<span style="color:red;padding-left:28px;">Babysitterjob aktualisiert.</span>';
		}
		
		if( $_POST['subject'] == 'delete' )
		{
			$job_id = $_GET['job_id'];
	
			$dbJob->Delete($job_id);
			
			echo '<span style="color:red;padding-left:28px;">Babysitterjob gelöscht.</span>';
		}
	}
}
else if (!empty($_GET) && array_key_exists('job_id', $_GET))
{
	$job_id = $_GET['job_id'];

	if($job_id != '')
		$job = $dbJob->Read($job_id);
	
	if($job->user_id != $user_id)
	{
		$requiredDisabled = "disabled";
		$disabled = "disabled";
	}
}

//Gibt eine Uhrzeit-Auswahlbox zurück
function GetTimeSelectBox($control_id, $requiredDisabled, $selected_value)
{
	echo '<select id='.$control_id.' name='.$control_id.' '.$requiredDisabled.'>';
	for( $i=0; $i<24; $i++)
	{
		$strUhrzeit = "";
	
		if($i<10)
		{
			$strUhrzeit = '0'.$i.':00';
		}
		else
		{
			$strUhrzeit = $i.':00';
		}
		
		if( $strUhrzeit == $selected_value )
		{
			echo '<option value="'.$strUhrzeit.'" selected >'.$strUhrzeit.'</option>';
		}
		else
		{
			echo '<option value="'.$strUhrzeit.'">'.$strUhrzeit.'</option>';
		}
	}
	echo '</select>';
}

//Wertet das Postback-Ergebnis aus und gibt die Werte als XML zurück
function GetScheduleXML()
{
	$scheduleXML = new SimpleXMLElement("<schedule></schedule>");
	
	for( $i = 0; $i<7; $i++)
	{
		$daynrXML = $scheduleXML->addChild('daynr_'.$i);
		
		//neues Element - available-Status
		if (!empty($_POST) && array_key_exists('check_'.$i, $_POST))
		{
			$check = $_POST['check_'.$i];
			$availableXML = $daynrXML->addChild('available',$check);
		}
		else
		{
			$availableXML = $daynrXML->addChild('available','0');
		}
		
		//neues Element - Uhrzeit
		$uhrzeitXML = $daynrXML->addChild('uhrzeit');
		
		//neues Element - von Uhrzeit
		if (!empty($_POST) && array_key_exists('von_'.$i, $_POST))
		{
			$von = $_POST['von_'.$i];
			$vonUhrzeitXML = $uhrzeitXML->addChild('von',$von);
		}
		else
		{
			$vonUhrzeitXML = $uhrzeitXML->addChild('von','00:00');
		}

		//neues Element - bis Uhrzeit
		if (!empty($_POST) && array_key_exists('bis_'.$i, $_POST))
		{
			$bis = $_POST['bis_'.$i];
			$bisUhrzeitXML = $uhrzeitXML->addChild('bis',$bis);
		}
		else
		{
			$bisUhrzeitXML = $uhrzeitXML->addChild('bis','00:00');
		}
	}
	
	return $scheduleXML->asXML();
}

//Gibt aus der XML-Struktur für eine TagNummer die Verfügbarkeit aus
function GetAvailableElementValue($xml, $index)
{
	if(!empty($xml))
	{
		$xml = simplexml_load_string($xml);
		if( $xml->{'daynr_'.$index}->available == "on")
			echo "checked";
	}
}

//Gibt aus der XML-Struktur für eine TagNummer die Von-Uhrzeit aus
function GetUhrzeitVonElementValue($xml, $index)
{
	if(!empty($xml))
	{
		$xml = simplexml_load_string($xml);
		return $xml->{'daynr_'.$index}->uhrzeit->von;
	}
	else
	{
		return '00:00';
	}
}

//Gibt aus der XML-Struktur für eine TagNummer die Bis-Uhrzeit aus
function GetUhrzeitBisElementValue($xml, $index)
{
	if(!empty($xml))
	{
		$xml = simplexml_load_string($xml);
		return $xml->{'daynr_'.$index}->uhrzeit->bis;
	}
	else
	{
		return '00:00';
	}
}

?>
<h3 id="content_headline">Job</h3>

<article>
<form method="post" action="form_jobs.php<?php echo '?job_id='.$job->job_id.'&user_id='.$user_id; ?>" onsubmit="return checkFormJobs()">

  <label>Betreff:</label>
  <input type="text" id="job_subject" name="job_subject" value="<?php echo $job->subject; ?>" <?php echo $requiredDisabled;?>>	

  <label>Zeitplan:</label>
  <table id="jobs_shedule" name="jobs_shedule" cellpadding="0" cellspacing="0">
		<tr>
			<td></td>
			<td>Montag</td>
			<td>Dienstag</td>
			<td>Mittwoch</td>
			<td>Donnerstag</td>
			<td>Freitag</td>
			<td>Samstag</td>
			<td>Sonntag</td>
		</tr>
		<tr>
			<td style="text-align: left;">verfügbar</td>
			<td><input id="check_0" name="check_0" type="checkbox" <?php echo $disabled.' '; GetAvailableElementValue($job->schedule,0); ?>></td>
			<td><input id="check_1" name="check_1" type="checkbox" <?php echo $disabled.' '; GetAvailableElementValue($job->schedule,1); ?>></td>
			<td><input id="check_2" name="check_2" type="checkbox" <?php echo $disabled.' '; GetAvailableElementValue($job->schedule,2); ?>></td>
			<td><input id="check_3" name="check_3" type="checkbox" <?php echo $disabled.' '; GetAvailableElementValue($job->schedule,3); ?>></td>
			<td><input id="check_4" name="check_4" type="checkbox" <?php echo $disabled.' '; GetAvailableElementValue($job->schedule,4); ?>></td>
			<td><input id="check_5" name="check_5" type="checkbox" <?php echo $disabled.' '; GetAvailableElementValue($job->schedule,5); ?>></td>
			<td><input id="check_6" name="check_6" type="checkbox" <?php echo $disabled.' '; GetAvailableElementValue($job->schedule,6); ?>></td>
		</tr>
		<tr>
			<td style="text-align: left;">von</td>
			<td><?php GetTimeSelectBox('von_0',$disabled, GetUhrzeitVonElementValue($job->schedule,0)); ?></td>
			<td><?php GetTimeSelectBox('von_1',$disabled, GetUhrzeitVonElementValue($job->schedule,1)); ?></td>
			<td><?php GetTimeSelectBox('von_2',$disabled, GetUhrzeitVonElementValue($job->schedule,2)); ?></td>
			<td><?php GetTimeSelectBox('von_3',$disabled, GetUhrzeitVonElementValue($job->schedule,3)); ?></td>
			<td><?php GetTimeSelectBox('von_4',$disabled, GetUhrzeitVonElementValue($job->schedule,4)); ?></td>
			<td><?php GetTimeSelectBox('von_5',$disabled, GetUhrzeitVonElementValue($job->schedule,5)); ?></td>
			<td><?php GetTimeSelectBox('von_6',$disabled, GetUhrzeitVonElementValue($job->schedule,6)); ?></td>
		</tr>
		<tr>
			<td style="text-align: left;">bis</td>
			<td><?php GetTimeSelectBox('bis_0',$disabled, GetUhrzeitBisElementValue($job->schedule,0)); ?></td>
			<td><?php GetTimeSelectBox('bis_1',$disabled, GetUhrzeitBisElementValue($job->schedule,1)); ?></td>
			<td><?php GetTimeSelectBox('bis_2',$disabled, GetUhrzeitBisElementValue($job->schedule,2)); ?></td>
			<td><?php GetTimeSelectBox('bis_3',$disabled, GetUhrzeitBisElementValue($job->schedule,3)); ?></td>
			<td><?php GetTimeSelectBox('bis_4',$disabled, GetUhrzeitBisElementValue($job->schedule,4)); ?></td>
			<td><?php GetTimeSelectBox('bis_5',$disabled, GetUhrzeitBisElementValue($job->schedule,5)); ?></td>
			<td><?php GetTimeSelectBox('bis_6',$disabled, GetUhrzeitBisElementValue($job->schedule,6)); ?></td>
		</tr>
  </table>
  <br>
  <label>Details:</label>
  <textarea id="details" name="details" cols="50" rows="10" <?php echo $requiredDisabled;?>><?php echo $job->details; ?></textarea>	
  
  <label>Anzahl der Kinder:</label>
  <input type="text" id="numberOfChildren" name="numberOfChildren" value="<?php echo $job->numberOfChildren; ?>" <?php echo $requiredDisabled;?>>	

  <button name="subject" type="submit" value="create" <?php echo $disabled;?>>Job anlegen</button>
  <button name="subject" type="submit" value="update" <?php echo $disabled;?>>Job speichern</button>
  <button name="subject" type="submit" value="delete" <?php echo $disabled;?>>Job l&ouml;schen</button>
  
</form>
</article>
<?php
require("../../requirements/sites/footer.php");
?>