<!--
	@author: Irina Novotny, Mathias Helms, Maximilian Wulf
	@Funktion: Erstellt eine Liste aller Jobs.
-->
<?php
require("../../requirements/sites/head.php");
?>
<?php
require("../../requirements/dal/database.php");
require("../../requirements/dal/jobs/DBJob.php");
require("../../requirements/dal/jobs/Job.php");

$job = new Job();

$dbJob = new DBJob();

$user_id = '';

if (!empty($_SESSION["user_id"]))
{
	$user_id = $_SESSION["user_id"];
}

$arrJobList = $dbJob->ReadAllJobs();

?>
<h3 id="content_headline">Babysitter gesucht</h3> 
<article>
<?php
if ($user_id != '')
{
	echo '<a id="anlegen" href="form_jobs.php">Neuen Job anlegen</a>';
}
?>
<ul>
<?php
if ($arrJobList != '')
{
	for($i = 0;$i < count($arrJobList); $i++)
	{
		$job = $arrJobList[$i];
		
		echo '<div name="jobContainer" jobid="'.$job->job_id.'" onClick="onJobClick(this, event);"><li class="clearfix">';
		echo '<h3 class="subject">'.$job->subject.'</h3>';
		if($job->user_id == $user_id)
		{
			echo '<img class="EditIcon" src="../../requirements/style/img/edit.png"/>';
		}
		else if(!empty($user_id))
		{
			echo '<div name="messageSendIcon" userid="'.$job->user_id.'" onClick="onJobClick(this, event);"><img class="MessageIcon" src="../../requirements/style/img/send.png" /></div>';
		}
		echo '<p class="desc">Beschreibung:</br>'.$job->details.'</p>';
		echo '<p class="numberOfChildren">Anzahl der Kinder: '.$job->numberOfChildren.'</p></br>';
		
		echo '</li></div>';
	}
}
?>
</ul>
</br>
</article>


<?php
require("../../requirements/sites/footer.php");
?>