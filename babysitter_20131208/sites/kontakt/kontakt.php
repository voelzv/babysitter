<!-- @author: Irina Novotny, Mathias Helms, Maximilian Wulf
			@Funktion: Kontaktformular zum Schreiben einer Nachricht,
			&ouml;ffnet sich mit Klick auf MessageButton in Job-Liste
			checkFormKontakt() pr&uuml;ft die Eingaben uaf ihre Vollst&auml;ndigkeit
			
-->


<?php
require("../../requirements/sites/head.php");
?>
<h3 id="content_headline">Kontakt</h3>
<script type="text/javascript">
function checkFormKontakt() {
  var Fehler='';

  if (document.forms[0].subject.value=="")
    Fehler += "Bitte Betreff eingeben\n";

  if (document.forms[0].body.value=="")
    Fehler += "Bitte Betreff eingeben\n";
	
  if (Fehler.length>0) 
  {
    alert("Bitte vollstaendig ausfuellen: \n\n"+Fehler);

    return(false);
  }

}

</script>
<article>
<p>	Haben Sie Fragen, Anregungen oder Kritik?
	Dann schreiben Sie uns bitte eine Nachricht!
</p>

<form action="mailto:downfall_75@yahoo.de"  methode = "get" onsubmit="return checkFormKontakt()">


<label>Betreff:</label><input name="subject" type="text" size="30" maxlength="30"></p>

<label>Ihre Nachricht:</label>
   <textarea name="body" rows="10" cols="50"></textarea>
	<label for="senden"> </label>
    <input id="senden" type="submit" name="senden" value="Senden" />
	<label for="loeschen"> </label>
    <input id="loeschen" type="reset" name="loeschen" value="Formular l&ouml;schen" />

</form>
</article>
<?php
require("../../requirements/sites/footer.php");
?>