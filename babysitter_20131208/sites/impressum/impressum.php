<!--
	@author: Maximilian Wulf
	@Funktion: Seite mit Impressum.
-->
<?php
require("../../requirements/sites/head.php");
?>
<h3 id="content_headline">Impressum</h3>
<article>
<h3>Das Babysitterportal</h3>
</br>
Gesch&auml;ftsf&uuml;hrung:</br>
Max Mustermann</br>
</br>
Anschrift:</br>
Musterstra&szlig;e 10</br>
D 22222 Musterstadt</br>
</br>
Telefon:</br>
+49 (0)321 654987</br>
</br>
Fax:</br>
+49 (0)321 654978</br>
</br>
E-Mail:</br>
<a href="mailto:info@babysitter.de">info@babysitter.de</a></br>
</br>
Internet:</br>
<a href="../start/index.php">www.babysitter.de</a></br>
</br>
Handelsregister Amtsgericht Musterstadt:</br>
AAA 9999</br>
</br>
Umsatzsteuer-Identifikationsnummer gem&auml;&szlig; &sect; 27 a Umsatzsteuergesetz:</br>
DE 999999999</br>
</br>
Gestaltung und Programmierung:</br>
Irina-Larissa Novotny, Mathias Helms, Maximilian Wulf
</article>
<?php
require("../../requirements/sites/footer.php");
?>