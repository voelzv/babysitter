<!--
	@author: Irina Novotny, Maximilian Wulf
	@Funktion: Startseite. Willkommenstext f&uuml;r die Besucher.
				Allgemeine Informationen &uuml;ber die Funktionen der Webpr&auml;senz
-->
<?php
require("../../requirements/sites/head.php");
?>

<h3 id="content_headline">Startseite</h3> 
<article>
<p>
<span  id="willkommenstext"><strong>HERZLICH WILLKOMMEN BEIM BABYSITTERPORTAL!</strong></span><br>
</p>
<div style="background-image:url(../../requirements/style/img/babysitter_bg_heart.jpg)">
 <p>
 <h2>Sie suchen einen Babysitter?! Sie brauchen ein Kinderm&auml;dchen?!</h2><br>
 <span >Dann sind Sie hier <strong>genau</strong> richtig</span><br>
 </p>
 <p>
 <img src="../../requirements/style/img/baby_and_bear.jpg" width="300" height="200" align="left" alt="Baby_Bear">
	<h3> Sind Sie Babysitter?? </h3>
			Sie sind Babysitter und suchen einen Job?
			Auf unserer umfangreichen 
			<a href= "../../sites/jobs/show_job_list.php">Jobliste</a><!-- Link zur Jobliste-->
			finden Sie den passenden f&uuml;r sich!<br>
			
			<h3> Suche Sie einen Babysitter??</h3>
			Hier k&ouml;nnen Sie auch Ihre
			<a href= "../../sites/jobs/show_job_list.php">Jobangebote</a><!-- Link zur Jobliste-->
			erstellen und nach einem Babysitter suchen!  <br><br>                      
</p>
<p> <img src="../../requirements/style/img/baby_feet.jpg" width="285" height="158" align="right" alt="Baby_Feet">
Es ist ganz einfach! 
	  Wenn Sie als Babysitter einen Job oder als Familie einen Babysitter suchen, registrieren Sie sich einfach <a href="../users/form_users.php">hier.</a><br>
	  Suchen Sie in unserer Jobliste nach dem f&uuml;r Sie richtigen Angebot.<br>
	  Oder erstellen Sie Ihr eigens Jobangebot  und finden so den richtigen Babysitter.<br><br>
	  Sie k&ouml;nnen ganz unkompliziert mit den Anbietern in Kontakt treten, indem Sie unsere Funktion zum Nachrichten senden nutzen.<br><br><br>
</p><p>

</p>
</article>
<?php
require("../../requirements/sites/footer.php");
?>