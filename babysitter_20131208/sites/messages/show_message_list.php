<!--
	@author: Irina Novotny, Mathias Helms, Maximilian Wulf
	@Funktion: Erstellt eine Liste aller Nachrichten zu einem User.
-->
<?php
require("../../requirements/sites/head.php");
?>
<?php
require("../../requirements/dal/database.php");
require("../../requirements/dal/messages/DBMessage.php");
require("../../requirements/dal/messages/Message.php");
require("../../requirements/dal/users/DBUser.php");
require("../../requirements/dal/users/User.php");

$dbMessage = new DBMessage();
$dbUser = new DBuser();

$user_id = '';

if (!empty($_SESSION["user_id"]))
{
	$user_id = $_SESSION["user_id"];

	$arrMessageList = $dbMessage->ReadMessageList($user_id);
	
	if (!empty($_GET) && array_key_exists('del_message_id', $_GET))
	{
		$del_message_id = $_GET['del_message_id'];
		
		//L&ouml;scht ausgew&auml;hlte Nachricht
		$dbMessage->Delete($del_message_id, $user_id);
		
		header("Location: show_message_list.php");
	}
}
?>
<h3 id="content_headline">Nachrichten</h3> 
<article>
<ul>
<?php
if ($arrMessageList != '')
{
	for($i = 0;$i < count($arrMessageList); $i++)
	{
		$message = $arrMessageList[$i];
		
		//Ermittlung des Benutzers der die Nachricht verfasst hat
		$user = $dbUser->Read($message->vonUser_id);
		
		echo '<div name="messageContainer" messageid="'.$message->message_id.'" onClick="onMessageClick(this, event);"><li class="clearfix_message">';
		echo '<h3 class="subject" style="color: black; font-weight: normal">'.$message->createDate.' - von: '.$user->name.' - </h3><h3 class="subject">'.$message->subject.'</h3>';	
		echo '<div name="messageDeleteIcon" messageid="'.$message->message_id.'" onClick="onMessageClick(this, event);"><img class="MessageIcon" src="../../requirements/style/img/delete.png" title="Nachricht löschen" /></div>';
		echo '</li></div>';
	}
}
else
{
		echo '<div name="messageContainer"><li class="clearfix_message">';
		echo '<h3 class="subject">Du hast keine Nachrichten</h3>';		
		echo '</li></div>';
}
?>
</ul>
</br>
</article>


<?php
require("../../requirements/sites/footer.php");
?>