﻿<!-- 
	@author: 	Irina Novotny, Maximilian Wulf, Mathias Helms
	@Funktion:	Datensatz Message wird angelegt.
				Kontakt-Formular das sich bei Click auf Button in Job-Liste &ouml;ffnet.
				Die Methode checkFormMessage() (-> function.js) pr&uuml;ft die Formulareingaben auf Vollst&auml;ndigkeit.
				
-->
<?php
require("../../requirements/sites/head.php");
?>
<?php
require("../../requirements/dal/database.php");
require("../../requirements/dal/messages/DBMessage.php");
require("../../requirements/dal/messages/Message.php");

$message = new Message();

$dbMessage = new DBMessage();

$anUser_id = '';
$vonUser_id = '';

if (!empty($_SESSION["user_id"]))
{
	$vonUser_id = $_SESSION["user_id"];
}

if (!empty($_POST) && array_key_exists('action', $_POST))
{
	if( $_POST['action'] == 'create' )
	{
		$message->vonUser_id = $_POST['vonUser_id'];       
		$message->anUser_id = $_POST['anUser_id'];       
		$message->subject = $_POST['subject'];
		$message->message = $_POST['message'];      
				
		$dbMessage->Create($message);
		
		echo "Die Nachricht wurde gesendet";
	}
}
else if (!empty($_GET) && array_key_exists('anUser_id', $_GET))
{
	$anUser_id = $_GET['anUser_id'];
}
else if (!empty($_GET) && array_key_exists('message_id', $_GET))
{
	$message_id = $_GET["message_id"];
	
	$message = $dbMessage->Read($message_id);
}
?>

<h3 id="content_headline">Nachricht senden</h3>

<article>
<form id="messageForm" method="post" action="form_messages.php" onsubmit="return checkFormMessage()">

	<input name="vonUser_id" id="vonUser_id" type="text" size="30" maxlength="30" value="<?php if($vonUser_id != ''){ echo $vonUser_id; }else{ echo $message->vonUser_id; } ?>" style="display:none;">
	
	<input name="anUser_id" id="anUser_id" type="text" size="30" maxlength="50" value="<?php if($anUser_id != ''){ echo $anUser_id; }else{ echo $message->anUser_id; } ?>" style="display:none;">
	
	<label <?php if($anUser_id != ''){ echo 'style="display:none;"'; }?>>Datum:</label>
	<label <?php if($anUser_id != ''){ echo 'style="display:none;"'; }?>><?php echo $message->createDate; ?></label>
	<br>
	<br>
	
	<label>Betreff:</label>
	<?php if($anUser_id == '')
	{
		echo '<span>'.$message->subject.'</span><br><br>';
	}
	else
	{
		echo '<input name="subject" id="subject" type="text" size="30" maxlength="30" required value="'.$message->subject.'">';
	}
	?>
		
	<label>Ihre Nachricht:</label>
	<?php if($anUser_id == '')
	{
		echo '<span>'.$message->message.'</span><br><br>';
	}
	else
	{
		echo '<textarea name="message" id="message" required rows="10" cols="50">'.$message->message.'</textarea>';
	}
	?>
    <button name="action" type="submit" value="create" <?php if($anUser_id == ''){ echo 'disabled'; }?>>Senden</button>
	
	<?php
	if ($anUser_id == '')
	{
		echo '<a href="form_messages.php?anUser_id='.$message->vonUser_id.'">Antworten</a>';
	}
	?>

</form>
</article>
<?php
require("../../requirements/sites/footer.php");
?>