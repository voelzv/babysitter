<!--
	@author: Mathias Helms, Maximilian Wulf
	@Funktion: Stellt eine Form bereit mit der User angelegt, verändert oder gelöscht werden können.
-->
<?php
require("../../requirements/sites/head.php");
?>
<?php
require("../../requirements/dal/database.php");
require("../../requirements/dal/users/DBUser.php");
require("../../requirements/dal/users/User.php");

$user = new User();

$dbUser = new DBUser();

if (!empty($_POST) && array_key_exists('subject', $_POST))
{
	if( $_POST['subject'] == 'create' )
	{
		//Prüfung ob Benutzername schon existiert
		$existUsername = $dbUser->ExistUserName($_POST['name']);
	
		if(!$existUsername)
		{
			$user->name = $_POST['name'];       
			$user->password = $_POST['password'];       
			$user->firstname = $_POST['firstname'];
			$user->lastname = $_POST['lastname'];   
			$user->city = $_POST['city'];      
			$user->street = $_POST['street'];    
			$user->number = $_POST['number'];    
			$user->zipcode = $_POST['zipcode'];
			$user->email = $_POST['email_addr'];
			$user->phone = $_POST['phone'];
			
			$dbUser->Create($user);
			
			echo '<span style="color:red;padding-left:28px;">Ihr Profil wurde angelegt. Sie können sich jetzt mit Ihren Benutzernamen und Passwort anmelden.</span>';
		}
		else
		{
			echo '<span style="color:red;padding-left:28px;">Der ausgewählte Benutzername existiert schon. Bitte wählen Sie einen anderen aus.</span>';
		}
	}
	
	if (!empty($_GET) && array_key_exists('user_id', $_GET))
	{	
		if( $_POST['subject'] == 'update' )
		{
			$user->user_id = $_GET['user_id'];
			$user->name = $_POST['name'];       
			$user->password = $_POST['password'];       
			$user->firstname = $_POST['firstname'];
			$user->lastname = $_POST['lastname'];   
			$user->city = $_POST['city'];      
			$user->street = $_POST['street'];    
			$user->number = $_POST['number'];    
			$user->zipcode = $_POST['zipcode'];
			$user->email = $_POST['email_addr'];
			$user->phone = $_POST['phone'];
			
			$dbUser->Update($user);

			echo '<span style="color:red;padding-left:28px;">Ihr Profil wurde aktualisiert.</span>';
		}
		
		if( $_POST['subject'] == 'delete' )
		{
			$user_id = $_GET['user_id'];
			$name = $_POST['name'];       
			$password = $_POST['password'];
			
			
			$isDeleted = $dbUser->Delete($user_id, $name, $password);
			
			if($isDeleted)
			{
				$logout = "../../requirements/sites/logout.php";
				
				header("Location: ".$logout);
				exit;
			}
			else
			{
				echo '<span style="color:red;padding-left:28px;">Ihr Profil konnte nicht gelöscht. Stellen Sie sicher, dass der Benutzername und das Passwort richtig sind</span>';
			}
		}
	}
}
else if (!empty($_SESSION["user_id"]))
{
	$user_id = $_SESSION["user_id"];
	
	$user = $dbUser->Read($user_id);
}

?>

<h3 id="content_headline">Profil</h3>

<article>
<form method="post" action="form_users.php<?php echo '?user_id='.$user->user_id; ?>" onsubmit="return checkForm()">

  <label>Benutzername:</label>
  <input type="text" id="name" name="name" value="<?php echo $user->name; ?>" required>	
  
  <label>Passwort:</label>
  <input type="password" id="password" name="password" required>
  
  <label>Passwort wiederholen:</label>
  <input type="password" id="password_repeat" name="password_repeat" required 
   oninput="check(this,'password')">
  
  <label>Vorname:</label>
  <input type="text" id="firstname" name="firstname" placeholder="Thomas" value="<?php echo $user->firstname; ?>" required>

  <label>Nachname:</label>
  <input type="text" id="lastname" name="lastname" placeholder="Schmidt" value="<?php echo $user->lastname; ?>" required>

  <label>E-Mail-Adresse:</label>
  <input type="email" id="email_addr" name="email_addr" value="<?php echo $user->email; ?>" required>
  
  <label>E-Mail-Adresse wiederholen:</label>
  <input type="email" id="email_addr_repeat" name="email_addr_repeat" value="<?php echo $user->email; ?>" required 
   oninput="check(this,'email_addr')">
	
  <label>Wohnort:</label>
  <input type="text" id="city" name="city" placeholder="Hamburg" value="<?php echo $user->city; ?>" required>
  
  <label>Straße:</label>
  <input type="text" id="street" name="street" placeholder="Musterstra&szlig;e" value="<?php echo $user->street; ?>" required>
  
  <label>Nummer:</label>
  <input type="text" id="number" name="number" placeholder="5c" value="<?php echo $user->number; ?>" required>
  
  <label>Postleitzahl:</label>
  <input type="text" id="zipcode" name="zipcode" placeholder="12345" value="<?php echo $user->zipcode; ?>" required>
  
  <label>Telefon:</label>
  <input type="text" id="zipcode" name="phone" value="<?php echo $user->phone; ?>">
  
  <button name="subject" type="submit" value="create">Profil anlegen</button>
  <?php
  if(!empty($user_id))
  {
	echo '<button name="subject" type="submit" value="update">Profil speichern</button>';
	echo '<button name="subject" type="submit" value="delete">Profil l&ouml;schen</button>';
  }
  ?>
  
</form>
</article>

<?php
require("../../requirements/sites/footer.php");
?>